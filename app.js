const express = require("express")
const axios = require("axios")
const cors = require("cors")

const app = express()
app.use(express.json())
app.use(cors())

app.get("/", (req, res) => {
  try {
    res.json({ status: "ok" })
  } catch (err) {
    console.log(err)
  }
})

app.post("/px", async (req, res) => {
  try {
    const fetchData = await axios(req.body)
    res.json(fetchData.data)
  } catch (err) {
    console.log(err)
    await axios({
      method: "post",
      header: {
        accept: "application/json",
      },
      url:
        "https://hooks.slack.com/services/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",
      data: {
        attachments: [
          {
            title: `AXIOS-PROXY ERR: ${
              err.message || err.msg || err.toString()
            }`,
          },
        ],
      },
    })
  }
})

const PORT = 3006

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`)
})
